from collections import Counter

def open_read_file():
    infile = r"./input/log.txt"

    with open(infile) as log:
        log = log.readlines()
    return log

log_file = open_read_file()
row_taker = [row.split() for row in log_file]

from analyzer import TimeSelect
obj = TimeSelect()

counter = 0

if obj == row_taker[0]:
    counter += 1
print(counter)
 #   if row_taker[1] == 'ComponentA':
  #      counter += 1
  #      print(counter)
