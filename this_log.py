#!/usr/bin/python

def generator_read_file_line_by_line():
    """
    Reading file line by line and strip() them from infile
    """
    infile = "./input/log.txt"
    with open(infile) as log_file:
        for line in log_file:
            yield line.strip()

"""
Variables
"""
gen = generator_read_file_line_by_line()
time = ''
res_d = {}

for line in gen:

    splitted_line = line.split()

    separator = ' : '
    cur_time  = splitted_line[0][:-4]
    component = splitted_line[1] + separator + splitted_line[2]
    record_type = splitted_line[3]
    component_and_record_type = component + separator + record_type

    if not time:
        time = cur_time

    if time == cur_time:
        """
        NOTE!!!! - in log file all date should be sorted! or this method will not work
        """
        if res_d.has_key(component):
            res_d[component] += 1
            res_d[component_and_record_type] = 1
        else:
            res_d[component] = 1
            res_d[component_and_record_type] = 1
    else:
        print "\nTime:", time
        for key, val in res_d.items():
            print '{} - {}'.format(key, val)
        res_d = {}
        time = ''