from datetime import datetime
from itertools import groupby
from collections import Counter

#Opening log file
def open_read_file():
    infile = r"./input/log.txt"

    with open(infile) as log:
        log = log.readlines()
    return log

log_file = open_read_file()
row_taker = [row.split() for row in log_file]

def print_only_time():
    for var in row_taker:
        print(var[0])

#print_only_time()

list = row_taker

"""
for k,g in groupby(list, key=lambda item: item[0][0:8]):
    print(k)
    print('\n'.join(x[0] for x in g) + '\n')
"""

class TimeSelect(object):
    def solve(item):
        for line in list:
            if not line: continue
            else:
                dt = datetime.strptime(item[0], '%H:%M:%S.%f')
                return dt.hour, dt.minute, dt.second

    for k, g in groupby(list, key=solve):
        outLine = ('\n'.join(x[0][0:8] for x in g) + '\n')

timeSelectedObjects = TimeSelect()

"""
def string_to_datetime(log):
    parts = log.split('.')
    var_date = datetime.strptime(parts[0], "%H:%M:%S.%f")
    return var_date.replace(microsecond=int(parts[1]))

def parsing_keywords(log):
    important = []
    keep_phrases = ['ComponentA']

    parsing_keywords = (line for line in log_file if any(phrase in line for phrase in keep_phrases))

    for line in log:
        for phrase in keep_phrases:
            if phrase in line:
                important.append(line)
                break

    return important

print parsing_keywords(log_file)


def time_cutter(var_date):
    time = datetime.strptime(var_date, '%H:%M:%S')
    start_timestamp = datetime.strptime(var_date, '%H:%M:%S')
    end_timestamp = datetime.strptime(var_date, '%H:%M:%S')

    if time >= start_timestamp and  time <= end_timestamp: print 'it worked'
"""

